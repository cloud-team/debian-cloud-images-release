# Release handling for Debian Cloud images

## How to make a release

* Run a pipeline on the `master` branch via [webinterface](https://salsa.debian.org/cloud-team/debian-cloud-images-release/pipelines/new), specifying following variables:

| Name | Description
|---|---|
| `PIPELINE_RELEASE_BULLSEYE` | Set to upload `bullseye`.
| `PIPELINE_RELEASE_BULLSEYE_BACKPORTS` | Set to upload `bullseye-backports`.
| `PIPELINE_VERSION` | The image version to release, e.g. `20190718-412`.

* After receiving the finished notification from Microsoft, manually run the `golive` job.

## Variables

### Per project

Environment variables needs to be configured in the GitLab web interface.

 * `$UPLOAD_SSH_KEY`
 * `$UPLOAD_SSH_REMOTE`

### Per trigger

* `$DIST`: Must be `buster`
* `$VERSION`
